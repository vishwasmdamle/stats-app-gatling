package com.sample.statsapp.simulations;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class StatsAppSimulation extends Simulation {

    private val httpConfiguration = http.baseURL("http://localhost:8080") //Make it configurable
    private val amountRandom = scala.util.Random
    private val timestampRandom = scala.util.Random

    private val transactionInputScenario = scenario("Transaction hits")
    .exec(
        http("Transaction entry")
        .post("/transactions")
        .header("Content-Type", "application/json")
        .body(StringBody(_ => "{\"amount\": " + amountRandom.nextInt(5000) + ", \"timestamp\": " + (System.currentTimeMillis() - timestampRandom.nextInt(2000)) + "}")).asJSON
    )

    private val statsQueryScenario = scenario("Stats Fetch")
    .exec(
        http("Stats query")
        .get("/stats")
        .header("Content-Type", "application/json")
    )

    setUp(
        transactionInputScenario.inject(
            nothingFor(5 seconds),
            rampUsers(500) over (5 second),
            constantUsersPerSec(100) during (15 seconds),
            rampUsersPerSec(150) to 1500 during (10 second),
            constantUsersPerSec(200) during (20 seconds),
            nothingFor(5 seconds),
            constantUsersPerSec(800) during (15 seconds),
            constantUsersPerSec(10) during (15 seconds)
        ).protocols(httpConfiguration),
        statsQueryScenario.inject(
            nothingFor(2 seconds),
            rampUsers(10) over (8 second),
            constantUsersPerSec(10) during (15 seconds),
            nothingFor(5 seconds),
            constantUsersPerSec(50) during (5 seconds),
            constantUsersPerSec(1) during (55 seconds)
        ).protocols(httpConfiguration)
    )
}
